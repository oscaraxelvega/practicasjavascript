var arreglo = [1, 2, 3, 4, 2 , 2, 2, 9];

function mostrarArray(arr){
    const num = document.getElementById('generados');
    while (num.options.length > 0) {
        num.remove(0);
    }
    for(con = 0; con <arr.length; ++con) {
        let option = document.createElement('option');
        option.value = arr[con];
        option.innerHTML = arr[con];
        num.appendChild(option);     
    }
}

function promedio(arr){
    let pro = 0;
    for(con = 0; con <arr.length; ++con) {
        pro += arr[con];   
    }
    pro /= arr.length;
    document.getElementById('promedio').textContent = "Promedio: "+pro.toFixed(2);
}

function mayor(arr) {
    let may = arr[0];
    let i = 0;
    for(con = 0; con <arr.length; ++con) {
        if(arr[con] > may){
            may = arr[con];
            i = con;
        }   
    }
    document.getElementById('max').textContent = "Mayor: "+may+" Índice: "+i;
}

function menor(arr) {
    let min = arr[0];
    let i = 0;
    for(con = arr.length; con > 0; --con) {
        if(arr[con] < min){
            min = arr[con];
            i = con;
        }   
    }
    document.getElementById('min').textContent = "Menor: "+min+" Índice: "+i;
}

function porcentajes(arr){
    let par = 0;
    for(con = 0; con <arr.length; ++con) {
        if (arr[con] % 2 == 0) {
            par++;
        }  
    }
    let porPar = par/arr.length*100;
    let porImpar = 100-(par/arr.length*100);
    document.getElementById('porPar').textContent = "Porcentaje de Pares: "+porPar.toFixed(2)+"%";
    document.getElementById('porImpar').textContent = "Porcentaje de Impares: "+porImpar.toFixed(2)+"%";
    if(Math.abs(porPar-porImpar) <= 20){
        document.getElementById('simetrico').textContent = "Simétrico: Sí";
    } else {
        document.getElementById('simetrico').textContent = "Simétrico: No";
    }
}

function rng(){
    const x = document.getElementById('num').value;
    var arr = [];
    for(con = 0; con < x; ++con){
        arr[con] = Math.floor(Math.random() * 1000);
    }
    return arr;
}

function generar(){
    var arr = rng();
    mostrarArray(arr);
    promedio(arr);
    mayor(arr);
    menor(arr);
    porcentajes(arr);
}
function calcularIMC() {
    const altura = parseFloat(document.getElementById("altura").value);
    const peso = parseFloat(document.getElementById("peso").value);
    
    if (!isNaN(altura) && !isNaN(peso)) {
        const imc = peso / (altura * altura);
        document.getElementById("resultado").textContent = imc.toFixed(2);
    } else {
        alert("Ingresa valores válidos para altura y peso.");
    }
}

function limpiar(){
    document.getElementById("altura").value = "";
    document.getElementById("peso").value = "";
    document.getElementById("resultado").textContent = "";
}